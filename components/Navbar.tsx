import { Navbar, Nav, Header } from "rsuite";
import { useRouter } from "next/router";
import { NextLink } from "./NextLink";
import { BsLightbulb, BsLightbulbOff } from "react-icons/bs";

type NavItem = {
  pageName: string;
  path: string;
};

interface Props {
  brand: string;
  items: NavItem[];
  theme: "light" | "dark";
  setTheme: (theme: "light" | "dark") => void;
}

const NavBar = ({ brand, items, theme, setTheme }: Props): JSX.Element => {
  const router = useRouter();

  const renderThemeButton = () => {
    if (theme == "light") {
      return (
        <Nav.Item icon={<BsLightbulb />} onClick={() => setTheme("dark")} />
      );
    }
    return (
      <Nav.Item icon={<BsLightbulbOff />} onClick={() => setTheme("light")} />
    );
  };

  return (
    <Header>
      <Navbar appearance="subtle">
        <Navbar.Brand as={NextLink} href="/">
          <h5>{brand}</h5>
        </Navbar.Brand>
        <Nav activeKey={router.pathname}>
          {items.map((item) => (
            <Nav.Item
              as={NextLink}
              eventKey={item.path}
              href={item.path}
              key={item.pageName}
            >
              {item.pageName}
            </Nav.Item>
          ))}
        </Nav>
        <Nav pullRight>{renderThemeButton()}</Nav>
      </Navbar>
    </Header>
  );
};

export default NavBar;
