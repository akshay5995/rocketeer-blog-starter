import React from "react";
import Link from "next/link";
import { NavItemProps } from "rsuite";

export const NextLink = React.forwardRef<HTMLAnchorElement, NavItemProps>(
  (props, ref) => {
    const { href, ...rest } = props;
    return (
      <Link href={href as any}>
        <a ref={ref} {...(rest as any)} />
      </Link>
    );
  }
);
