import { Tag, TagGroup } from "rsuite";

const tagColors = [
  "red",
  "yellow",
  "blue",
  "cyan",
  "orange",
  "violet",
  "green",
];

const getTagColor = (text: String) => {
  return tagColors[text.length % tagColors.length];
};

export const Tags = ({
  tags,
  align,
}: {
  tags?: string[];
  align?: "left" | "right";
}) => (
  <TagGroup style={{ margin: "8px", textAlign: align || "right" }}>
    {tags?.map((tag) => (
      <Tag size="md" key={tag} color={getTagColor(tag) as any}>
        {tag}
      </Tag>
    ))}
  </TagGroup>
);
