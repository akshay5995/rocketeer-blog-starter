import React from "react";
import FlexboxGrid from "rsuite/FlexboxGrid";
import { PostMetaData } from "../pages/types/common";
import { Tags } from "./Tags";
import Divider from "rsuite/Divider";

type Props = {
  metadata?: PostMetaData;
};

export const PostInfo: React.FunctionComponent<Props> = ({ metadata }) => {
  return (
    <>
      <FlexboxGrid.Item colspan={24}>
        <h2 className="title">{metadata?.title}</h2>
      </FlexboxGrid.Item>
      <FlexboxGrid.Item colspan={24}>
        <p className="description">published on: {metadata?.date}</p>
        <Tags tags={metadata?.tags} align="right" />
      </FlexboxGrid.Item>
      <Divider />
    </>
  );
};
