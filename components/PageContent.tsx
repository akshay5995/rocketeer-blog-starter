import React from "react";
import FlexboxGrid from "rsuite/FlexboxGrid";
import Placeholder from "rsuite/Placeholder";
import { Content } from "../pages/types/common";
import Divider from "rsuite/Divider";

type Props = {
  content?: Content;
  loading: boolean;
  errorMessage: string;
};

export const PageContent: React.FunctionComponent<Props> = ({
  content,
  loading,
  errorMessage,
}) => {
  const renderPlaceHolder = () => (
    <FlexboxGrid.Item colspan={24}>
      <Placeholder active />
    </FlexboxGrid.Item>
  );

  const renderContent = () => {
    if (!!errorMessage) {
      return <FlexboxGrid.Item colspan={24}>{errorMessage}</FlexboxGrid.Item>;
    }
    return (
      <>
        <FlexboxGrid.Item colspan={24}>
          <h2 className="title">{content?.title}</h2>
        </FlexboxGrid.Item>
        <Divider />
        <FlexboxGrid.Item colspan={24}>
          <p className="description">{content?.description}</p>
        </FlexboxGrid.Item>
      </>
    );
  };

  return loading ? renderPlaceHolder() : renderContent();
};
