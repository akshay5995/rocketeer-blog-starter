import type { NextPage } from "next";
import withLayout, { getStaticProps } from "./layout/withLayout";
import FlexboxGrid from "rsuite/FlexboxGrid";
import { useContent } from "./hooks/useContent";
import { PageContent } from "../components/PageContent";

const Home: NextPage = () => {
  const contentResponse = useContent("home");

  return (
    <FlexboxGrid>
      <FlexboxGrid.Item colspan={24}>
        <PageContent {...contentResponse} />
      </FlexboxGrid.Item>
    </FlexboxGrid>
  );
};

export { getStaticProps };

export default withLayout(Home);
