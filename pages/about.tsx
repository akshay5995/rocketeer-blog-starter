import type { NextPage } from "next";
import withLayout, { getStaticProps } from "./layout/withLayout";
import { useContent } from "./hooks/useContent";
import { PageContent } from "../components/PageContent";
import FlexboxGrid from "rsuite/FlexboxGrid";

const About: NextPage = () => {
  const contentResponse = useContent("about");

  return (
    <FlexboxGrid className="hero">
      <PageContent {...contentResponse} />
    </FlexboxGrid>
  );
};

export { getStaticProps };

export default withLayout(About);
