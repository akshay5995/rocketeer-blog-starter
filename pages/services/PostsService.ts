import { promises as fs } from "fs";
import path from "path";
import { Service } from "typedi";
import matter from "gray-matter";
import { PostMetaData } from "../types/common";
import { remark } from "remark";
import remarkHtml from "remark-html";

type Post = {
  metadata: PostMetaData;
  content?: string;
};

@Service()
class PostsService {
  private postsById = new Map();
  private postsMetaData = new Set();

  private readPostsAndCache = async () => {
    const postsDirectory = path.join(process.cwd(), "posts");
    const filenames = await fs.readdir(postsDirectory);

    const postsPromise = filenames.map(async (filename) => {
      const filePath = path.join(postsDirectory, filename);
      const str = await fs.readFile(filePath, "utf8");
      const { data, content } = matter(str);

      return {
        metadata: data,
        content,
      } as Post;
    });

    const posts: Post[] = await Promise.all(postsPromise);

    posts.forEach(async (post) => {
      const metadata = {
        ...post.metadata,
        date: new Date(post.metadata.date).toLocaleDateString(),
      };

      this.postsMetaData.add(metadata);

      const contentHtml = await remark()
        .use(remarkHtml)
        .process(post?.content || "");

      this.postsById.set(post.metadata.slug, {
        metadata,
        content: contentHtml.value,
      });
    });
  };

  getPostsMetadata = async () => {
    if (this.postsMetaData.size == 0) {
      await this.readPostsAndCache();
    }

    return this.postsMetaData;
  };

  getPostBySlug = async (slug: string) => {
    if (this.postsById.size == 0) {
      await this.readPostsAndCache();
    }

    return this.postsById.get(slug);
  };
}

export { PostsService };
