import { promises as fs } from "fs";
import path from "path";
import yaml from "yaml";
import { Service } from "typedi";
import { MetaData } from "../types/common";

@Service()
class ConfigService {
  private configCache: string = "";

  private getConfigurationFromYaml = async () => {
    await this.repopulateCache();
    return yaml.parse(this.configCache);
  };

  repopulateCache = async () => {
    const configFilePath = path.join(process.cwd(), "/config.yaml");

    if (!this.configCache) {
      const fileContent = await fs.readFile(configFilePath, "utf-8");
      this.configCache = fileContent;
    }
  };

  getMetaDataConfig = async (): Promise<MetaData> => {
    const config = await this.getConfigurationFromYaml();
    return { ...config.metadata };
  };

  getPageContentConfig = async (pageName: string) => {
    const config = await this.getConfigurationFromYaml();

    const contentConfig = config.content;

    return { ...contentConfig[pageName] };
  };
}

export { ConfigService };
