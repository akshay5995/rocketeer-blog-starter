interface ApiResponse {
  loading: boolean;
  errorMessage: string;
}

class Social {
  github?: string;
  linkedin?: string;
  twitter?: string;
}

class MetaData {
  name!: string;
  brand!: string;
  pages!: string[];
  social!: Social;
}

class Content {
  title?: string;
  description?: string;
}

class PostMetaData {
  title!: string;
  date!: string;
  enabled!: boolean;
  description!: string;
  slug!: string;
  tags?: string[];
}

class Post {
  metadata!: PostMetaData;
  content!: string;
}

export { MetaData, ApiResponse, Content, PostMetaData, Post };
