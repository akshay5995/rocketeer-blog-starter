import Head from "next/head";
import { CustomProvider, Container, Content, Footer } from "rsuite";
import NavBar from "../../components/Navbar";
import { MetaData } from "../types/common";
import IconButton from "rsuite/IconButton";
import { Icon } from "@rsuite/icons";
import {
  AiOutlineGithub,
  AiOutlineTwitter,
  AiOutlineLinkedin,
} from "react-icons/ai";
import { useLocalStorage } from "../hooks/useLocalStorage";

interface Props {
  metadata: MetaData;
  children: React.ReactNode;
}

const Layout = ({ metadata, children }: Props): JSX.Element => {
  const [theme, setTheme] = useLocalStorage<"light" | "dark">("theme", "dark");

  const navigationItems =
    metadata.pages.map((page) => ({
      pageName: page,
      path: `/${page.toLowerCase()}`,
    })) || [];

  const social = metadata.social;

  return (
    <CustomProvider theme={theme}>
      <Head>
        <title>{metadata.brand}</title>
      </Head>
      <Container className="layout-container">
        <NavBar
          brand={metadata.brand}
          items={navigationItems}
          theme={theme}
          setTheme={setTheme}
        />
        <Content className="content">{children}</Content>
        <Footer>
          <div>
            {social.github && (
              <IconButton
                icon={<Icon as={AiOutlineGithub} />}
                href={social.github}
                appearance="link"
              />
            )}
            {social.linkedin && (
              <IconButton
                icon={<Icon as={AiOutlineLinkedin} />}
                href={social.linkedin}
                appearance="link"
              />
            )}
            {social.twitter && (
              <IconButton
                icon={<Icon as={AiOutlineTwitter} />}
                href={social.twitter}
                appearance="link"
              />
            )}
          </div>
          <div>{`©2021-Present ${metadata?.brand}`}</div>
        </Footer>
      </Container>
    </CustomProvider>
  );
};

export default Layout;
