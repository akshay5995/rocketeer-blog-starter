import Layout from "./Layout";
import Error from "./Error";
import { MetaData } from "../types/common";
import { ConfigService } from "../services/ConfigService";
import type { GetStaticProps } from "next";

interface StaticProps {
  metadata?: MetaData;
  isNotFound: boolean;
}

export const getStaticProps: GetStaticProps = async () => {
  const metadata: MetaData = await new ConfigService().getMetaDataConfig();

  if (!metadata) {
    return {
      props: {
        metadata: undefined,
        isNotFound: true,
      },
    };
  }

  return {
    props: {
      metadata,
      isNotFound: false,
    },
  };
};

const withLayout =
  <P extends object>(Component: React.ComponentType<P>) =>
  (props: P & StaticProps): JSX.Element => {
    if (props.isNotFound || !props.metadata) {
      return (
        <Error>
          <div>Error loading page! :(</div>
        </Error>
      );
    }

    return (
      <Layout metadata={props.metadata}>
        <Component {...props} />
      </Layout>
    );
  };

export default withLayout;
