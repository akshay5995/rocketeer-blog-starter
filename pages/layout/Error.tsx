import Head from "next/head";
import { CustomProvider, Container, Content } from "rsuite";

interface Props {
  children: React.ReactNode;
}

const Error = ({ children }: Props): JSX.Element => {
  return (
    <CustomProvider theme="dark">
      <Head>
        <title>Error</title>
      </Head>
      <Container>
        <Content className="content">{children}</Content>
      </Container>
    </CustomProvider>
  );
};

export default Error;
