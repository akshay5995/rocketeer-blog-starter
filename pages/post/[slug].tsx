import type { GetStaticPaths, NextPage } from "next";
import withLayout, { getStaticProps } from "../layout/withLayout";
import Panel from "rsuite/Panel";
import Placeholder from "rsuite/Placeholder";
import FlexboxGrid from "rsuite/FlexboxGrid";
import { usePostData } from "../hooks/usePostData";
import { PostInfo } from "../../components/PostInfo";
import { useRouter } from "next/router";

const Post: NextPage = () => {
  const router = useRouter();
  const { slug } = router.query;
  const { loading, post, errorMessage } = usePostData(slug as string);

  const renderContent = () => {
    if (loading) {
      return (
        <Panel>
          <Placeholder.Paragraph />
        </Panel>
      );
    }

    if (!!errorMessage || !post) {
      return <p>{errorMessage || "No posts!!"}</p>;
    }

    const { metadata, content } = post;

    return (
      <FlexboxGrid.Item colspan={24}>
        <PostInfo metadata={metadata} />
        <div dangerouslySetInnerHTML={{ __html: content }}></div>
      </FlexboxGrid.Item>
    );
  };

  return <FlexboxGrid>{renderContent()}</FlexboxGrid>;
};

export const getStaticPaths: GetStaticPaths = async () => {
  return {
    paths: [{ params: { slug: "test" } }, { params: { slug: "hello" } }],
    fallback: false,
  };
};

export { getStaticProps };

export default withLayout(Post);
