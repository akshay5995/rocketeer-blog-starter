import useSWR from "swr";
import { Content } from "../types/common";
import { fetcher } from "../utils/fetcher";
import { Hooks } from "./types";
import { contentQuery } from "./graphql/queries";

type Data = {
  getPageContent: Content;
};

export const useContent = (pageName: string): Hooks.UseContentReponse => {
  const { data, error } = useSWR<Data, unknown>(
    contentQuery(pageName),
    fetcher
  );

  if (!data)
    return {
      content: undefined,
      loading: true,
      errorMessage: "",
    };

  if (error) {
    return {
      content: undefined,
      loading: false,
      errorMessage: "Can't fetch content",
    };
  }

  return {
    content: data.getPageContent,
    loading: false,
    errorMessage: "",
  };
};
