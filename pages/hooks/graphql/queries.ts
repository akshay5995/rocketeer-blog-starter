const metaDataQuery = "{ getMetaData { brand, name, pages } }";

const contentQuery = (pageName: string) =>
  `{ getPageContent(pageName: \"${pageName}\") { title, description } }`;

const postsQuery = `{
    getPosts {
    title,
    enabled,
    description,
    tags
    slug
  }
}`;

const postQuery = (slug: string) =>
  `{
    getPostById(slug: \"${slug}\") {
        metadata {
            title
            tags
            date
        }
        content
    }
  }`;

export { metaDataQuery, contentQuery, postsQuery, postQuery };
