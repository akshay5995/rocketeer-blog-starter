import useSWR from "swr";
import { MetaData } from "../types/common";
import { fetcher } from "../utils/fetcher";
import { Hooks } from "./types";
import { metaDataQuery } from "./graphql/queries";

type Data = {
  getMetaData: MetaData;
};

export const useMetaData = (): Hooks.UseMetaDataResponse => {
  const { data, error } = useSWR<Data, unknown>(metaDataQuery, fetcher);

  if (!data)
    return {
      metadata: undefined,
      loading: true,
      errorMessage: "",
    };

  if (error) {
    return {
      metadata: undefined,
      loading: false,
      errorMessage: "Can't fetch metadata",
    };
  }

  return {
    metadata: data.getMetaData,
    loading: false,
    errorMessage: "",
  };
};
