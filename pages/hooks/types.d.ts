import {
  ApiResponse,
  Content,
  MetaData,
  Post,
  PostMetaData,
} from "../types/common";

namespace Hooks {
  interface UseMetaDataResponse extends ApiResponse {
    metadata?: MetaData;
  }

  interface UseContentReponse extends ApiResponse {
    content?: Content;
  }

  interface UsePostMetaDataResponse extends ApiResponse {
    posts?: PostMetaData[];
  }

  interface UsePostDataResponse extends ApiResponse {
    post?: Post;
  }
}

export { Hooks };
