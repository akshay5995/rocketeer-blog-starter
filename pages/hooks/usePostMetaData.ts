import useSWR from "swr";
import { PostMetaData } from "../types/common";
import { fetcher } from "../utils/fetcher";
import { Hooks } from "./types";
import { postsQuery } from "./graphql/queries";

type Data = {
  getPosts: PostMetaData[];
};

export const usePostMetaData = (): Hooks.UsePostMetaDataResponse => {
  const { data, error } = useSWR<Data, unknown>(postsQuery, fetcher);

  if (!data)
    return {
      posts: undefined,
      loading: true,
      errorMessage: "",
    };

  if (error) {
    return {
      posts: undefined,
      loading: false,
      errorMessage: "Can't fetch metadata",
    };
  }

  return {
    posts: data.getPosts,
    loading: false,
    errorMessage: "",
  };
};
