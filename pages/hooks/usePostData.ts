import useSWR from "swr";
import { Post } from "../types/common";
import { fetcher } from "../utils/fetcher";
import { Hooks } from "./types";
import { postQuery } from "./graphql/queries";

type Data = {
  getPostById: Post;
};

export const usePostData = (id: string): Hooks.UsePostDataResponse => {
  const { data, error } = useSWR<Data, unknown>(postQuery(id), fetcher);

  if (!data)
    return {
      post: undefined,
      loading: true,
      errorMessage: "",
    };

  if (error) {
    return {
      post: undefined,
      loading: false,
      errorMessage: "Can't fetch metadata",
    };
  }

  return {
    post: data.getPostById,
    loading: false,
    errorMessage: "",
  };
};
