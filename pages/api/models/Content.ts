import { ObjectType, Field } from "type-graphql";

@ObjectType()
export class Content {
  @Field((type) => String, { nullable: true })
  title?: string;

  @Field((type) => String, { nullable: true })
  description?: string;
}
