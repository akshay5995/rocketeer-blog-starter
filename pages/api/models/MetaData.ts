import { ObjectType, Field } from "type-graphql";

@ObjectType()
export class Social {
  @Field((type) => String, { nullable: true })
  github?: string;
  @Field((type) => String, { nullable: true })
  linkedin?: string;
  @Field((type) => String, { nullable: true })
  twitter?: string;
}

@ObjectType()
export class MetaData {
  @Field((type) => String)
  name!: string;

  @Field((type) => String)
  brand!: string;

  @Field((type) => [String])
  pages!: string[];

  @Field((returns) => Social)
  social!: Social;
}
