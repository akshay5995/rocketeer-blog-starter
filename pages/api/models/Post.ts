import { ObjectType, Field } from "type-graphql";

@ObjectType()
export class PostMetaData {
  @Field((type) => String)
  title!: string;

  @Field((type) => String)
  date!: string;

  @Field((type) => Boolean)
  enabled!: boolean;

  @Field((type) => String)
  description!: string;

  @Field((type) => String)
  slug!: string;

  @Field((type) => [String], { nullable: true })
  tags?: string[];
}

@ObjectType()
export class Post {
  @Field((type) => PostMetaData)
  metadata!: PostMetaData;

  @Field((type) => String)
  content!: string;
}
