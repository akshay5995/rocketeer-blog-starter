import { Query, Resolver, Arg } from "type-graphql";
import { PostsService } from "../../services/PostsService";
import { Post } from "../models/Post";
import { Service } from "typedi";

@Service()
@Resolver((of) => Post)
export class PostResolver {
  constructor(private postService: PostsService) {}

  @Query((returns) => Post)
  async getPostById(@Arg("slug") slug: string) {
    const post = await this.postService.getPostBySlug(slug);

    if (post === undefined) {
      throw new Error("Couldn't get posts");
    }
    return post;
  }
}
