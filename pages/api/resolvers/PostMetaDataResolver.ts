import { Query, Resolver, Arg } from "type-graphql";
import { PostsService } from "../../services/PostsService";
import { PostMetaData } from "../models/Post";
import { Service } from "typedi";

@Service()
@Resolver((of) => PostMetaData)
export class PostMetaDataResolver {
  constructor(private postService: PostsService) {}

  @Query((returns) => [PostMetaData])
  async getPosts() {
    const posts = await this.postService.getPostsMetadata();

    if (posts === undefined) {
      throw new Error("Couldn't get posts");
    }
    return posts;
  }
}
