import { Query, Resolver } from "type-graphql";
import { ConfigService } from "../../services/ConfigService";
import { MetaData } from "../models/MetaData";
import { Service } from "typedi";

@Service()
@Resolver((of) => MetaData)
export class MetaDataResolver {
  constructor(private configService: ConfigService) {}

  @Query(() => MetaData)
  async getMetaData() {
    const metadata = await this.configService.getMetaDataConfig();
    if (metadata === undefined) {
      throw new Error("Metadata not found");
    }
    return metadata;
  }
}
