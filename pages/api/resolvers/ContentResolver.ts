import { Query, Resolver, Arg } from "type-graphql";
import { ConfigService } from "../../services/ConfigService";
import { Content } from "../models/Content";
import { Service } from "typedi";

@Service()
@Resolver((of) => Content)
export class ContentResolver {
  constructor(private configService: ConfigService) {}

  @Query((returns) => Content)
  async getPageContent(@Arg("pageName") pageName: string) {
    const content = await this.configService.getPageContentConfig(pageName);

    if (content === undefined) {
      throw new Error(`Content not found for page: ${pageName}`);
    }
    return content;
  }
}
