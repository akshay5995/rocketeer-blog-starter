import "reflect-metadata";
import Cors from "micro-cors";
import type { PageConfig } from "next";
import { ApolloServer } from "apollo-server-micro";
import { buildSchema } from "type-graphql";
import { MetaDataResolver } from "./resolvers/MetaDataResolver";
import { ContentResolver } from "./resolvers/ContentResolver";
import { PostMetaDataResolver } from "./resolvers/PostMetaDataResolver";
import { Container } from "typedi";
import { PostResolver } from "./resolvers/PostResolver";

export const config: PageConfig = {
  api: {
    bodyParser: false,
  },
};

const cors = Cors();

export default cors(async (req, res) => {
  if (req.method === "OPTIONS") {
    res.end();
    return false;
  }

  const server = new ApolloServer({
    schema: await buildSchema({
      resolvers: [
        MetaDataResolver,
        ContentResolver,
        PostMetaDataResolver,
        PostResolver,
      ],
      container: Container,
    }),
  });

  await server.start();
  await server.createHandler({ path: "/api/graphql" })(req, res);
});
