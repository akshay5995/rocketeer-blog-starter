import type { NextPage } from "next";
import withLayout, { getStaticProps } from "./layout/withLayout";
import { usePostMetaData } from "./hooks/usePostMetaData";
import PanelGroup from "rsuite/PanelGroup";
import Panel from "rsuite/Panel";
import Placeholder from "rsuite/Placeholder";
import { useContent } from "./hooks/useContent";
import { PageContent } from "../components/PageContent";
import { Tags } from "../components/Tags";
import FlexboxGrid from "rsuite/FlexboxGrid";
import Button from "rsuite/Button";

const Blog: NextPage = () => {
  const { loading, posts, errorMessage } = usePostMetaData();
  const content = useContent("posts");

  const renderContent = () => {
    if (loading) {
      return (
        <Panel>
          <Placeholder.Paragraph />
        </Panel>
      );
    }

    if (!!errorMessage || !posts) {
      return <p>{errorMessage || "No posts!!"}</p>;
    }

    return posts
      .filter((post) => post.enabled)
      .map((post) => (
        <Panel key={post.title} header={post.title}>
          {post.description}
          <Tags tags={post?.tags || []} />
          <Button appearance="link" block href={`/post/${post.slug}`}>
            Read more!
          </Button>
        </Panel>
      ));
  };

  return (
    <FlexboxGrid>
      <FlexboxGrid.Item colspan={24}>
        <PageContent {...content} />
      </FlexboxGrid.Item>
      <FlexboxGrid.Item colspan={24}>
        <PanelGroup bordered accordion>
          {renderContent()}
        </PanelGroup>
      </FlexboxGrid.Item>
    </FlexboxGrid>
  );
};

export { getStaticProps };

export default withLayout(Blog);
